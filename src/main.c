#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "pgpcat.h"

ssize_t
read_cb(void *cookie, void *buf, size_t count)
{
  ssize_t read = 0;
  do
    {
      ssize_t r = fread(buf + read, 1, count - read, stdin);
      read += r;
    }
  while (ferror(stdin) == EINTR && read < count);

  return read;
}

ssize_t
write_cb(void *cookie, const void *buf, size_t count)
{
  ssize_t wrote = 0;
  do
    {
      ssize_t w = fwrite(buf + wrote, 1, count - wrote, stdout);
      wrote += w;
    }
  while (ferror(stdout) == EINTR && wrote < count);

  return wrote;
}

int
main(int argc, char *argv[])
{
  char *filename = "stdin";

  if (argc > 2)
    {
      fprintf(stderr, "Usage: %s [FILE.PGP] > literal-data\n", argv[0]);
      exit(1);
    }
  else if (argc == 2)
    {
      filename = argv[1];
      int fd = open(filename, O_RDONLY);
      if (fd == -1)
        {
          fprintf(stderr, "Opening '%s': %s\n", filename, strerror(errno));
          exit(1);
        }
      if (fd != STDIN_FILENO)
        {
          if (dup2(fd, STDIN_FILENO) == -1)
            {
              fprintf(stderr, "Redirecting %s to stdin: %s\n",
                      filename, strerror(errno));
              exit(1);
            }
        }
    }

  if (pgpcat(NULL, read_cb, write_cb) == -1)
    {
      fprintf(stderr, "Extracting data from %s: %s\n",
              filename, strerror(errno));
      exit(1);
    }

  return 0;
}
