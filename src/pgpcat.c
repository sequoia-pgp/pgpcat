#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <endian.h>
#include <inttypes.h>

#include "pgpcat.h"

// Whether to enable debugging output.
#define DEBUG 0
#if DEBUG > 0
#  define debug(fmt, ...)                                        \
  do                                                             \
    {                                                            \
      const char *__d_fn = __builtin_strrchr (__FILE__, '/');    \
      fprintf (stderr, "%s:%s:%d (%"PRIxPTR"): "fmt,             \
               __d_fn ? __d_fn + 1 : __FILE__,                   \
               __func__, __LINE__,                               \
               (uintptr_t) __builtin_return_address (0),         \
               ##__VA_ARGS__);                                   \
    }                                                            \
  while (0)
#else
#  define debug(fmt, ...)                       \
  do                                            \
    {                                           \
    }                                           \
  while (0)
#endif

enum packet_type
  {
    PACKET_TYPE_RESERVED = 0,
    PACKET_TYPE_PKESK = 1,
    PACKET_TYPE_SIGNATURE = 2,
    PACKET_TYPE_SKESK = 3,
    PACKET_TYPE_ONE_PASS_SIG = 4,
    PACKET_TYPE_SECRET_KEY = 5,
    PACKET_TYPE_PUBLIC_KEY = 6,
    PACKET_TYPE_SECRET_SUBKEY = 7,
    PACKET_TYPE_COMPRESSED_DATA = 8,
    PACKET_TYPE_SED = 9,
    PACKET_TYPE_MARKER = 10,
    PACKET_TYPE_LITERAL = 11,
    PACKET_TYPE_TRUST = 12,
    PACKET_TYPE_USERID = 13,
    PACKET_TYPE_PUBLIC_SUBKEY = 14,
    PACKET_TYPE_USER_ATTRIBUTE = 17,
    PACKET_TYPE_SEIP = 18,
    PACKET_TYPE_MDC = 19,
    PACKET_TYPE_AED = 20,
  };

static const char *packet_type_string(enum packet_type packet_type)
  __attribute__ ((unused));

static const char *packet_type_string(enum packet_type packet_type)
{
  switch (packet_type)
    {
    case PACKET_TYPE_RESERVED:
      return "reserved";
    case PACKET_TYPE_PKESK:
      return "PKESK";
    case PACKET_TYPE_SIGNATURE:
      return "signature";
    case PACKET_TYPE_SKESK:
      return "SKESK";
    case PACKET_TYPE_ONE_PASS_SIG:
      return "OPS";
    case PACKET_TYPE_SECRET_KEY:
      return "secret key";
    case PACKET_TYPE_PUBLIC_KEY:
      return "public key";
    case PACKET_TYPE_SECRET_SUBKEY:
      return "secret subkey";
    case PACKET_TYPE_COMPRESSED_DATA:
      return "compressed data";
    case PACKET_TYPE_SED:
      return "SED";
    case PACKET_TYPE_MARKER:
      return "marker";
    case PACKET_TYPE_LITERAL:
      return "literal";
    case PACKET_TYPE_TRUST:
      return "trust";
    case PACKET_TYPE_USERID:
      return "userid";
    case PACKET_TYPE_PUBLIC_SUBKEY:
      return "public subkey";
    case PACKET_TYPE_USER_ATTRIBUTE:
      return "user attribute";
    case PACKET_TYPE_SEIP:
      return "SEIP";
    case PACKET_TYPE_MDC:
      return "MDC";
    case PACKET_TYPE_AED:
      return "AED";
    default:
      return "unknown";
    }
}

// An OpenPGP Header Length.
//
// https://tools.ietf.org/html/rfc4880#section-4.2
struct length {
  bool partial_body;
  uint32_t bytes;
};

// Parses a new format length encoding.  Fills in *LENGTH.
//
// Returns false and sets errno if stdin does not contain a valid
// header or an error occurs while reading from stdin.
//
// Returns true if a header is successfully read and parsed.
static bool
read_new_format_length(void *cookie, read_cb_t read_cb, struct length *length)
{
  int r;
  uint8_t octet1;
  r = read_cb(cookie, &octet1, 1);
  if (r != 1)
    return 0;

  if (octet1 <= 191)
    // One octet encoding.
    {
      length->bytes = octet1;
      length->partial_body = false;
    }
  else if (octet1 <= 223)
    // Two octet encoding.
    {
      uint8_t octet2;
      r = read_cb(cookie, &octet2, 1);
      if (r == 0)
        {
          if (feof(stdin))
            errno = EPROTO;
          return 0;
        }

      length->bytes = ((octet1 - 192) << 8) + octet2 + 192;
      length->partial_body = false;
    }
  else if (octet1 <= 254)
    // Partial body length.
    {
      length->bytes = 1 << (octet1 & 0x1F);
      length->partial_body = true;
    }
  else
    // Five octet encoding.
    {
      uint32_t buffer;
      size_t r = read_cb(cookie, (char *) &buffer, 4);
      if (r != 4)
        {
          if (feof(stdin))
            errno = EPROTO;
          return false;
        }

      length->bytes = be32toh(buffer);
      length->partial_body = false;
    }

  return true;
}

struct header {
  int packet_type;
  struct length length;
};


// Parses an OpenPGP packet's header.  Fills in *HEADER.
//
// Returns false and sets errno if the input does not contain a valid
// header or an error occurs while reading from stdin.  If EOF is
// encountered at any other point other than the first byte of the
// header, sets errno to EPROTO.
//
// Returns true if a header is successfully read and parsed.
//
// Note: because we only handle signed messages, we return an error on
// indeterminate lengths (NOT partial body lengths).
static bool
read_header(void *cookie, read_cb_t *read_cb, struct header *header)
{
  uint8_t ptag;

  size_t r = read_cb(cookie, &ptag, 1);
  if (r == 0)
    // EOF.  We didn't read anything so don't force errno to an error
    // code.
    return false;

  debug("ptag: %x\n", ptag);
  if (!(ptag & (1 << 7)))
    {
      // The MSb must be 1.
      errno = EPROTO;
      return false;
    }

  bool new_format = ptag & (1 << 6);
  debug("format: %s\n", new_format ? "new" : "old");
  if (new_format)
    {
      header->packet_type = ptag & 0x3F;
      debug("packet type: 0x%x\n", header->packet_type);
      if (!read_new_format_length(cookie, read_cb, &header->length))
        {
          if (! errno)
            errno = EPROTO;
          return false;
        }
      else
        return true;
    }
  else
    {
      header->packet_type = (ptag >> 2) & 0xF;
      debug("packet type: 0x%x\n", header->packet_type);

      int length_type = ptag & 0x3;
      debug("length type: %x\n", length_type);

      if (length_type == 0)
        {
          uint8_t octet1;
          r = read_cb(cookie, &octet1, 1);
          if (r != 1)
            {
              if (feof(stdin))
                errno = EPROTO;
              return 0;
            }

          header->length.bytes = octet1;
        }
      else if (length_type == 1)
        {
          uint8_t octets[2];
          r = read_cb(cookie, &octets, 2);
          if (r != 2)
            {
              if (feof(stdin))
                errno = EPROTO;
              return 0;
            }

          header->length.bytes = (octets[0] << 8) | (octets[1]);
        }
      else if (length_type == 2)
        {
          uint8_t octets[4];
          r = read_cb(cookie, &octets, 4);
          if (r != 4)
            {
              if (feof(stdin))
                errno = EPROTO;
              return 0;
            }

          header->length.bytes =
            (octets[0] << 8) | (octets[1])
            | (octets[2] << 8) | (octets[3]);
        }
      else if (length_type == 3)
        // We don't support indeterminate packet lengths.
        {
          errno = EPROTO;
          return false;
        }

      return true;
    }
}

int
pgpcat(void *cookie, read_cb_t *read_cb, write_cb_t *write_cb)
{
  // Accept empty messages.
  bool saw_a_packet = false;

  while (true)
    {
      struct header header;
      if (! read_header(cookie, read_cb, &header))
        {
          if (feof(stdin))
            // We've read the whole message, but we didn't see a
            // literal data packet.  That's an error.
            {
              if (! saw_a_packet)
                // Empty message.
                break;

              debug("EOF without seeing a literal data packet\n");
              errno = EPROTO;
              return -1;
            }

          debug("Error reading packet header: %s\n",
                strerror(ferror(stdin)));
          errno = EPROTO;
          return -1;
        }
      else
        {
          debug("Read packet header! %s (%d), %s, %"PRIu32"\n",
                packet_type_string(header.packet_type),
                header.packet_type,
                header.length.partial_body ? "partial" : "full",
                header.length.bytes);
        }

      saw_a_packet = true;

      // We don't want to emit the literal packet's metadata.
      bool skipped_header = false;
      while (true)
        {
          while (header.length.bytes > 0)
            {
              char buffer[64 * 4096];
              int try = sizeof(buffer);
              if (try > header.length.bytes)
                try = header.length.bytes;

              int r = read_cb(cookie, buffer, try);
              if (r == 0)
                {
                  debug("Read error: %s\n", strerror(errno));
                  return -1;
                }

              if (header.packet_type == PACKET_TYPE_LITERAL)
                {
                  debug("Found a literal data packet\n");

                  int skip = 0;
                  if (!skipped_header)
                    {
                      // type: 1 byte
                      // filename len (n): 1 byte
                      // filename: n bytes
                      // creation time: 4 bytes
                      if (r < 6)
                        {
                          debug("Truncated literal data packet header "
                                "(got %d bytes)\n",
                                r);
                          errno = EPROTO;
                          return -1;
                        }

                      skip = 6 + buffer[1];
                      if (r < skip)
                        {
                          debug("Truncated literal data packet header "
                                "(got %d bytes, expected: %d)\n",
                                r, skip);
                          errno = EPROTO;
                          return -1;
                        }

                      skipped_header = true;
                    }

                  int w = write_cb(cookie, &buffer[skip], try - skip);
                  if (w != try - skip)
                    {
                      debug("Short write: %s", strerror(errno));
                      return -1;
                    }
                }

              header.length.bytes -= try;
            }

          if (header.length.partial_body)
            // Read the length of the next chunk.
            {
              read_new_format_length(cookie, read_cb, &header.length);
            }
          else
            {
              // End of the last chunk.
              debug("End of packet %s (%d)\n",
                    packet_type_string(header.packet_type),
                    header.packet_type);
              break;
            }
        }

      if (header.packet_type == PACKET_TYPE_LITERAL)
        // We're done.
        break;
    }

  return 0;
}

