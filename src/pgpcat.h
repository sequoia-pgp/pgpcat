#ifndef PGPCAT_H
#define PGPCAT_H

#include <stdio.h>

// Callback that has the same semantics as the `read` system call
// except that a short read is only permissible if an error occurs or
// EOF is reached.  Note: this means that the callback must also
// handle EAGAIN.
typedef ssize_t read_cb_t(void *cookie, void *buf, size_t count);

// This callback has the same semantics as the `write` system call
// except that a short write is only permissible if an error occurs.
// Note: this means that the callback must also handle EAGAIN.
typedef ssize_t write_cb_t(void *cookie, const void *buf, size_t count);

// Reads data by calling `read_cb` with `cookie` and writes by calling
// `write_cb` with `cookie`.
//
// This function returns the content of the first OpenPGP literal data
// packet found in the message.  It ignores all other packets, and it
// does not handle any container packets (in particular, it doesn't
// decompress data).  It also doesn't verify that the message is well
// formed according to Section 11.3 of RFC 4880:
//
//   https://tools.ietf.org/html/rfc4880#section-11.3
//
// This function also returns an error if there is no literal data
// packet.
//
// On success, 0 is returned.
//
// On error, -1 is returned and errno is set.
int pgpcat(void *cookie, read_cb_t *read_cb, write_cb_t *write_cb);

#endif
