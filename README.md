pgpcat is a simple program to extract the data from an OpenPGP
message.  This program does not do any decryption nor does it verify
signatures.  In fact, it can't even decompress any compressed data.

pgpcat is a first step towards allowing data to be signed inline by
default.  Distributing signatures inline makes it much easier to
verify signatures, because there is only a single file to download,
and there is no need to determine what the signature file is called
(foo.sig? foo.sign?  foo.asc?) or whether the signature data is over
the compressed or uncompressed data (for instance, the linux kernel
distributes linux.tar.gz, but the signature is over linux.tar).
