#! /bin/sh

set -e

if test x$# != x2
then
    echo "Usage: $0 path/to/pgpcat path/to/testfile"
    exit 1
fi

pgpcat=$1
echo "pgpcat: $pgpcat"
file=$2
echo "file: $file"

# Strip leading directories and the descriptive suffix (everything
# after the first dash).
result=$(echo $file | sed 's#^.*/##; s/-.*$//')

if test "x$result" = xfail
then
    echo expecting failure...
    if $pgpcat < "$file"
    then
        echo "$file succeeded, but should have failed."
        exit 1
    fi
else
    bytes=$($pgpcat < "$file" | wc -c)
    if test "x$result" != "x$bytes"
    then
        echo "$file: Got $bytes, expected $result bytes."
        exit 1
    fi
fi

exit 0

